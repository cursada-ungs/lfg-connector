package lfg.connector;

import com.lfg.view.MainView;

import main.InitCore;

public class Connector {

	public static void main(String[] args) {

		String pluginPath = "plugins";
		String confPath = "conf/lfg.properties";
		
		try {
			InitCore init = new InitCore( pluginPath, confPath );
			
			new MainView( init.init() );
		} catch( Exception e ) {
			e.printStackTrace();
		}
	}

}
